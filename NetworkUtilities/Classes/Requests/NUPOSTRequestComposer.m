//
//  NUPOSTRequestComposer.m
//  NetworkUtilities
//
//  Created by Mike Ponomaryov on 25.05.15.
//  Copyright (c) 2015 bMuse. All rights reserved.
//

#import "NUPOSTRequestComposer.h"

#define K_DEFAULT_BOUNDARY  @"POSTBoundary"

@interface NUPOSTRequestComposer ()

@property (nonatomic, assign) NSStringEncoding  encoding;
@property (nonatomic, strong) NSMutableData     *bodyData;
@property (nonatomic, strong) NSString          *boundary;

@end

@interface NUPOSTRequestComposerImage ()

- (BOOL)isValid;
- (NSString *)contentType;

@end

@implementation NUPOSTRequestComposerImage

- (BOOL)isValid
{
    BOOL isValid = NO;
    
    if (self.data.length && self.name.length &&
        (self.type == NUPOSTRequestComposerImageTypePNG || self.type == NUPOSTRequestComposerImageTypeJPEG))
    {
        isValid = YES;
    }
    return isValid;
}

- (NSString *)contentType
{
    NSString *contentType = @"image";
    
    if (self.type == NUPOSTRequestComposerImageTypeJPEG)
    {
        contentType = @"image/jpeg";
    }
    else if (self.type == NUPOSTRequestComposerImageTypePNG)
    {
        contentType = @"image/png";
    }
    
    return contentType;
}

@end

@implementation NUPOSTRequestComposer

- (instancetype)init
{
    return [self initWithBoundary:K_DEFAULT_BOUNDARY encoding:NSUTF8StringEncoding];
}

- (instancetype)initWithBoundary:(NSString *)boundary encoding:(NSStringEncoding)encoding
{
    self = [super init];
    if (self)
    {
        self.encoding = encoding ? encoding : NSUTF8StringEncoding;
        self.bodyData = [NSMutableData data];
        self.boundary = boundary.length ? boundary : K_DEFAULT_BOUNDARY;
    }
    return self;
}

- (void)addText:(NSString *)text forKey:(NSString *)key
{
    if (text.length == 0 || key.length == 0)
    {
        return;
    }
    
    [self.bodyData appendData:[self _dataFromStringWithFormat:@"--%@\r\n", self.boundary]];
    [self.bodyData appendData:[self _dataFromStringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key]];
    [self.bodyData appendData:[self _dataFromStringWithFormat:@"%@\r\n", text]];
}

- (void)addImage:(NUPOSTRequestComposerImage *)image forKey:(NSString *)key
{
    NSData *imageBodyData = [self _dataFromImage:image forKey:key];
    if (imageBodyData)
    {
        [self.bodyData appendData:imageBodyData];
    }
}

- (void)addImageFromPath:(NSString *)imagePath forKey:(NSString *)key
{
    if (!imagePath || !key || imagePath.length == 0 || key.length == 0)
    {
        return;
    }
    
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:imagePath]];
    NSString *fileName = imagePath.lastPathComponent;
    NSString *fileExt = imagePath.pathExtension.lowercaseString;
    
    NUPOSTRequestComposerImage *image = [NUPOSTRequestComposerImage new];
    image.data = imageData;
    image.name = fileName;
    image.type = [fileExt isEqualToString:@"png"] ? NUPOSTRequestComposerImageTypePNG : ([fileExt isEqualToString:@"jpg"] || [fileExt isEqualToString:@"jpeg"]) ? NUPOSTRequestComposerImageTypeJPEG : NUPOSTRequestComposerImageTypeUnknown;
    
    NSData *imageBodyData = [self _dataFromImage:image forKey:key];
    if (imageBodyData)
    {
        [self.bodyData appendData:imageBodyData];
    }
}

- (void)addParametersFromDictionary:(NSDictionary <NSString *, NSObject *> *)parameters
{
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSObject * _Nonnull obj, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:NSString.class])
        {
            NSString *text = (NSString *)obj;
            [self addText:text forKey:key];
        }
        else if ([obj isKindOfClass:NSNumber.class])
        {
            NSNumber *number = (NSNumber *)obj;
            [self addText:number.stringValue forKey:key];
        }
        else if ([obj isKindOfClass:NUPOSTRequestComposerImage.class])
        {
            NUPOSTRequestComposerImage *image = (NUPOSTRequestComposerImage *)obj;
            [self addImage:image forKey:key];
        }
    }];
}

- (NSData *)body
{
    NSMutableData *body = [self.bodyData mutableCopy];
    [body appendData:[self _dataFromStringWithFormat:@"--%@--\r\n\0", self.boundary]];
    
    return body;
}

+ (NSString *)boundary
{
    return K_DEFAULT_BOUNDARY;
}

#pragma mark - Private

- (NSData *)_dataFromImage:(NUPOSTRequestComposerImage *)image forKey:(NSString *)key
{
    NSMutableData *imageData = nil;
    
    if (image.isValid && key.length)
    {
        imageData = [NSMutableData data];
        
        NSString *contentType = [NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", image.contentType];
        
        [imageData appendData:[self _dataFromStringWithFormat:@"--%@\r\n", self.boundary]];
        [imageData appendData:[self _dataFromStringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", key, image.name]];
        [imageData appendData:[self _dataFromStringWithFormat:contentType]];
        [imageData appendData:image.data];
        [imageData appendData:[self _dataFromStringWithFormat:@"\r\n"]];
    }
    
    return imageData;
}

- (NSData *)_dataFromStringWithFormat:(NSString *)format, ...
{
    NSData *data = nil;

    if (format)
    {
        va_list argList;
        va_start(argList, format);
        
        NSString *str = [[NSString alloc] initWithFormat:format arguments:argList];
        data = [str dataUsingEncoding:self.encoding];
        
        va_end(argList);

    }
    
    return data;
}

@end
