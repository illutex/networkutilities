//
//  JSONDataRequest.m
//  NetworkUtilities
//
//  Created by Mike Ponomaryov on 07.08.2018.
//  Copyright © 2018 Mike Ponomaryov. All rights reserved.
//

#import "NUJSONDataRequest.h"

@implementation NUJSONDataRequest

- (instancetype)initWithURL:(NSURL *)requestURL
{
    self = [super initWithURL:requestURL];
    if (self)
    {
        self.contentType = @"application/json";
    }
    return self;
}

- (id)prepareReceivedData:(id)data
{
    id dataToSend = nil;
    
    NSError *jsonParsingError = nil;
    NSDictionary *parsedJSONResponseData = [NSJSONSerialization JSONObjectWithData:data
                                                                           options:0
                                                                             error:&jsonParsingError];
    if (jsonParsingError)
    {
        dataToSend = jsonParsingError;
    }
    else
    {
        NSError* responseError = [self _checkForErrorsInResponse:parsedJSONResponseData];
        dataToSend = responseError ? responseError : parsedJSONResponseData;
    }
    
    return dataToSend;
}

#pragma mark - Private

- (NSError *)_checkForErrorsInResponse:(id)response
{
    NSError* error = [NSError errorWithDomain:NSURLErrorDomain
                                         code:500
                                     userInfo:[NSDictionary dictionaryWithObject:@"Error has occured" forKey:NSLocalizedDescriptionKey]];
    
    if ([response isMemberOfClass:NSDictionary.class] || [response isMemberOfClass:NSArray.class])
    {
        // add additional checks here (if response format is specified) or in subclasses
    }
    else
    {
        error = nil;
    }
    
    return error;
}

@end
