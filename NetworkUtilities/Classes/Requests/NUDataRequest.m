//
//  DataRequest.m
//  NetworkUtilities
//
//  Created by Mike Ponomaryov on 07.08.2018.
//  Copyright © 2018 Mike Ponomaryov. All rights reserved.
//

#import "NUDataRequest.h"

#define kDefaultBoundary        @"POSTBoundary"
#define kNoDataErrorTitle       @"No data received"

@interface NUDataRequest ()

@property (nonatomic, strong)   NSURL                       *url;

@property (nonatomic, assign)   BOOL                        isExecuting;
@property (nonatomic, copy)     void                        (^completion)(id, NSError *);

@property (nonatomic, strong)   NSURLSession                *session;
@property (nonatomic, strong)   NSURLSessionDataTask        *dataTask;
@property (nonatomic, strong)   NSMutableURLRequest         *request;
@property (nonatomic, strong)   NSURLResponse               *response;

@end

@interface NUDataRequest (Memory)

- (void)hold;
- (void)unhold;

@end

static NSMutableArray <NUDataRequest *> *staticRequests = nil;

@implementation NUDataRequest

+ (instancetype)requestWithURL:(NSURL *)requestURL
{
    id request = [[self alloc] initWithURL:requestURL];
    return request;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self _defaultPreparations];
    }
    return self;
}

- (instancetype)initWithURL:(NSURL *)requestURL
{
    NSAssert(requestURL, @"Could not init request without URL");
    
    self = [super init];
    if (self)
    {
        [self _defaultPreparations];
        self.url = requestURL;
    }
    return self;
}

- (void)dealloc
{
    [self cancel];
}

- (void)setBody:(NSData *)body
{
    NSAssert(!self.isExecuting, @"Could not change body data when request already executing");
    
    if (_body != body)
    {
        _body = body;
        
        if (body.length && ![self _isBodyAvailableForMethod:self.requestMethod])
        {
            self.requestMethod = NURequestMethodPost;
        }
    }
}

- (void)setBoundary:(NSString *)boundary
{
    NSAssert(!self.isExecuting, @"Could not change boundary when request already executing");
    
    _boundary = boundary;
}

- (void)startWithCompletion:(void (^)(id response, NSError *error))completion
{
    NSAssert(self.url, @"Could not start request without url");
    
    if (self.isExecuting) return;
    
    self.isExecuting = YES;
    self.completion = completion;
    typeof(self) __weak weakSelf = self;
    
    [self hold];
    
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    self.request = [self _requestWithBodyData:self.body andBoundary:self.boundary];
    
    self.dataTask = [self.session dataTaskWithRequest:self.request
                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        weakSelf.response = response.copy;
        
        if (error)
        {
            if (weakSelf.completion) weakSelf.completion(nil, error);
        }
        else
        {
            if (weakSelf.dataTask.state == NSURLSessionTaskStateCompleted)
            {
                if (data.length)
                {
                    id response = [weakSelf prepareReceivedData:data];
                    if ([response isKindOfClass:NSError.class])
                    {
                        if (weakSelf.completion) weakSelf.completion(nil, response);
                    }
                    else
                    {
                        if (weakSelf.completion) weakSelf.completion(response, nil);
                    }
                }
                else
                {
                    NSDictionary *errorDict = [NSDictionary dictionaryWithObject:kNoDataErrorTitle
                                                                          forKey:NSLocalizedDescriptionKey];
                    NSError *noDataError = [NSError errorWithDomain:NSURLErrorDomain
                                                               code:500
                                                           userInfo:errorDict];
                    if (weakSelf.completion) weakSelf.completion(nil, noDataError);
                }
                
            }
        }
        
        weakSelf.dataTask = nil;
        [weakSelf.session finishTasksAndInvalidate];
        weakSelf.session = nil;
        
        weakSelf.isExecuting = NO;
        
        [weakSelf unhold];
    }];
    
    [self.dataTask resume];
}

- (id)prepareReceivedData:(id)data
{    
    return data;
}

- (void)cancel
{
    self.isExecuting = NO;
    [self.session invalidateAndCancel];
    self.session = nil;
    [self.dataTask cancel];
    self.dataTask = nil;
    self.request = nil;
}

#pragma mark - Private

- (NSMutableURLRequest *)_requestWithBodyData:(NSData *)bodyData andBoundary:(NSString *)boundary
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:self.url];
    
    request.HTTPMethod = [self _stringFromRequestMethod:self.requestMethod];
    
    if (bodyData.length && [self _isBodyAvailableForMethod:self.requestMethod])
    {
        request.HTTPBody = bodyData;
        
        NSString *contentLength = [NSString stringWithFormat:@"%lu", (unsigned long)bodyData.length];
        NSString *contentType = [NSString stringWithFormat:@"%@; charset=utf-8; boundary=%@", self.contentType, boundary];
        [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
        [request addValue:contentLength forHTTPHeaderField:@"Content-Length"];
    }
    

    [self.headerFields enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSString * _Nonnull obj, BOOL * _Nonnull stop) {
        [request addValue:obj forHTTPHeaderField:key];
    }];
    
    return request;
}

- (void)_defaultPreparations
{
    self.isExecuting = NO;
    self.boundary = kDefaultBoundary;
    self.requestMethod = NURequestMethodGet;
}

- (NSString *)_stringFromRequestMethod:(NURequestMethod)method
{
    NSString *methodStr = @"";
    
    switch (method)
    {
        case NURequestMethodGet:
            methodStr = @"GET";
            break;
        case NURequestMethodPost:
            methodStr = @"POST";
            break;
        case NURequestMethodPut:
            methodStr = @"PUT";
            break;
        case NURequestMethodPatch:
            methodStr = @"PATCH";
            break;
        case NURequestMethodDelete:
            methodStr = @"DELETE";
            break;
        case NURequestMethodCopy:
            methodStr = @"COPY";
            break;
        case NURequestMethodHead:
            methodStr = @"HEAD";
            break;
        case NURequestMethodOptions:
            methodStr = @"OPTIONS";
            break;
        case NURequestMethodLink:
            methodStr = @"LINK";
            break;
        case NURequestMethodUnlink:
            methodStr = @"UNLINK";
            break;
        case NURequestMethodPurge:
            methodStr = @"PURGE";
            break;
        case NURequestMethodLock:
            methodStr = @"LOCK";
            break;
        case NURequestMethodUnlock:
            methodStr = @"UNLOCK";
            break;
        case NURequestMethodPropfind:
            methodStr = @"PROPFIND";
            break;
        case NURequestMethodView:
            methodStr = @"VIEW";
            break;
            
        default:
            methodStr = self.body.length ? @"POST" : @"GET";
            break;
    }
    
    return methodStr;
}

- (BOOL)_isBodyAvailableForMethod:(NURequestMethod)method
{
    BOOL isBodyAvailable = NO;
    
    switch (method)
    {
        case NURequestMethodGet:
        case NURequestMethodCopy:
        case NURequestMethodHead:
        case NURequestMethodPurge:
        case NURequestMethodUnlock:
            isBodyAvailable = NO;
            break;
            
        case NURequestMethodPost:
        case NURequestMethodPut:
        case NURequestMethodPatch:
        case NURequestMethodDelete:
        case NURequestMethodOptions:
        case NURequestMethodLink:
        case NURequestMethodUnlink:
        case NURequestMethodLock:
        case NURequestMethodPropfind:
        case NURequestMethodView:
            isBodyAvailable = YES;
            break;
            
        default:
            isBodyAvailable = YES;
            break;
    }
    
    return isBodyAvailable;
}

@end

@implementation NUDataRequest (Memory)

- (void)hold
{
    @synchronized (staticRequests)
    {
        if (!staticRequests)
        {
            staticRequests = [NSMutableArray array];
        }
        [staticRequests addObject:self];
    }
}

- (void)unhold
{
    @synchronized (staticRequests)
    {
        [staticRequests removeObject:self];
        if (staticRequests.count == 0)
        {
            staticRequests = nil;
        }
    }
}

@end
