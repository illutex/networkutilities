//
//  NUPOSTRequestComposer.h
//  NetworkUtilities
//
//  Created by Mike Ponomaryov on 25.05.15.
//  Copyright (c) 2015 bMuse. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    NUPOSTRequestComposerImageTypeJPEG,
    NUPOSTRequestComposerImageTypePNG,
    NUPOSTRequestComposerImageTypeUnknown = 999,
} NUPOSTRequestComposerImageType;

@interface NUPOSTRequestComposerImage : NSObject

@property (nonatomic) NSData                            *data;
@property (nonatomic) NSString                          *name;
@property (nonatomic) NUPOSTRequestComposerImageType    type;

@end

@interface NUPOSTRequestComposer : NSObject

@property (readonly) NSString   *boundary;

- (instancetype)init;
- (instancetype)initWithBoundary:(NSString *)boundary encoding:(NSStringEncoding)encoding;

- (void)addText:(NSString *)text forKey:(NSString *)key;
- (void)addImage:(NUPOSTRequestComposerImage *)image forKey:(NSString *)key;
- (void)addImageFromPath:(NSString *)imagePath forKey:(NSString *)key;
- (void)addParametersFromDictionary:(NSDictionary <NSString *, NSObject *> *)parameters; //supported: NSString, NSNumber, NUPOSTRequestComposerImage

- (NSData*)body;

@end
