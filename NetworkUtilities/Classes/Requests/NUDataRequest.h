//
//  DataRequest.h
//  NetworkUtilities
//
//  Created by Mike Ponomaryov on 07.08.2018.
//  Copyright © 2018 Mike Ponomaryov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    NURequestMethodGet = 0,
    NURequestMethodPost,
    NURequestMethodPut,
    NURequestMethodPatch,
    NURequestMethodDelete,
    NURequestMethodCopy,
    NURequestMethodHead,
    NURequestMethodOptions,
    NURequestMethodLink,
    NURequestMethodUnlink,
    NURequestMethodPurge,
    NURequestMethodLock,
    NURequestMethodUnlock,
    NURequestMethodPropfind,
    NURequestMethodView
} NURequestMethod;

@interface NUDataRequest : NSObject

@property (nonatomic, readonly) NSURL                                   *url;
@property (nonatomic, strong)   NSData                                  *body;
@property (nonatomic, assign)   NURequestMethod                         requestMethod;
@property (nonatomic, strong)   NSDictionary <NSString *, NSString *>   *headerFields;
@property (nonatomic, strong)   NSString                                *contentType;
@property (nonatomic, strong)   NSString                                *boundary;
@property (nonatomic, readonly) NSURLResponse                           *response;

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)requestWithURL:(NSURL *)requestURL;
- (instancetype)initWithURL:(NSURL *)requestURL;
- (void)startWithCompletion:(void (^)(id response, NSError *error))completion;
- (void)cancel;

/*
 * Convinient method for preparing data structure for further actions
 * Subclasses could override this method to perform any data modifications.
 * After this modified data will be returned in completion handler.
 * Subclasses can also return NSError object if they don't receive expected values.
 */
- (id)prepareReceivedData:(id)data;

@end
