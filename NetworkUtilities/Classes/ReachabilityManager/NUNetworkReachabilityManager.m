//
//  NetworkReachabilityManager.m
//  NetworkUtilities
//
//  Created by Mike Ponomaryov on 07.08.2018.
//  Copyright © 2018 Mike Ponomaryov. All rights reserved.
//

#import "NUNetworkReachabilityManager.h"
#import "Reachability.h"
#import <SystemConfiguration/SCNetworkReachability.h>
#include <netinet/in.h>

#if T_MACOS
@import AppKit;
#endif

#if T_IOS
@import UIKit;
#endif

NSString *kNetworkReachabilityLost = @"NetworkReachabilityLostConnection";
NSString *kNetworkReachabilityRestored = @"NetworkReachabilityRestored";

#define kReachabilityMonitoringTimerInterval    3.0f
#define kReachabilityWaitingTimerInterval       5.0f
#define kReachabilityNoInternetTitle            @"No Internet connection detected."
#define kReachabilityNoInternetMessage          @"Please connect to the Internet and try again."

static NUNetworkReachabilityManager *sharedInstance = nil;

@interface NUNetworkReachabilityManager ()
{
    NSTimer     *_reachabilityWaitTimer;
    NSTimer     *_monitoringTimer;
    BOOL        _latestCheckWasReachable;
}

@end

@interface NUNetworkReachabilityManager (Private)

- (void)_startPendingTimer;
- (void)_stopPendingTimer;

@end

@implementation NUNetworkReachabilityManager

+ (NUNetworkReachabilityManager *)shared
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
    {
        sharedInstance = [NUNetworkReachabilityManager new];
    });
	
	return sharedInstance;
}

- (id)init 
{
    self = [super init];
    if (self) 
	{
        _latestCheckWasReachable = [NUNetworkReachabilityManager isConnectedToNetwork];
    }
    return self;
}

- (void)dealloc
{
    [_monitoringTimer invalidate];
    [_reachabilityWaitTimer invalidate];
}

#pragma mark - Public methods implementation
+ (BOOL)isConnectedToNetwork
{
    // Create zero addy
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
	
    // Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;
	
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
	
    if (!didRetrieveFlags)
    {
        printf("Error. Could not recover network reachability flags\n");
        return NO;
    }
    
    if (![[Reachability reachabilityForInternetConnection] isReachable])
    {
        return NO;
    }
	
    BOOL isReachable = flags & kSCNetworkFlagsReachable;
    BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    return (isReachable && !needsConnection) ? YES : NO;
}

- (BOOL)checkNetworkReachability
{
	BOOL isConnected = [NUNetworkReachabilityManager isConnectedToNetwork];
	
	if (!isConnected)
	{
		[self _startPendingTimer];
	}
	return isConnected;
}

- (void)beginMonitoring
{
    if(!_monitoringTimer)
    {
        _latestCheckWasReachable = [NUNetworkReachabilityManager isConnectedToNetwork];
        
        _monitoringTimer = [NSTimer scheduledTimerWithTimeInterval:kReachabilityMonitoringTimerInterval
                                                            target:self 
                                                          selector:@selector(_onTimer:)
                                                          userInfo:nil 
                                                           repeats:YES];
    }
}

- (void)finishMonitoring
{
    if(_monitoringTimer)
    {
        [_monitoringTimer invalidate];
        _monitoringTimer = nil;
    }
}

#pragma mark - Private methods implementation

- (void)_startPendingTimer
{
	[self _stopPendingTimer];
	
	_reachabilityWaitTimer = [NSTimer scheduledTimerWithTimeInterval:kReachabilityWaitingTimerInterval
															  target:self
                                                            selector:@selector(_onTimer:)
															userInfo:nil
															 repeats:NO];
}

- (void)_stopPendingTimer
{
	if (_reachabilityWaitTimer)
	{
		[_reachabilityWaitTimer invalidate];
		_reachabilityWaitTimer = nil;
	}
}

- (void)_onTimer:(NSTimer *)timer
{
    BOOL isNowReachable = [NUNetworkReachabilityManager isConnectedToNetwork];
    
    if ([timer isEqual:_reachabilityWaitTimer])
    {
        //for [self checkNetworkReachability]
        if (isNowReachable)
        {
			[self _stopPendingTimer];
            [[NSNotificationCenter defaultCenter] postNotificationName:kNetworkReachabilityRestored
                                                                object:nil];
            if (self.changeBlock)
            {
                self.changeBlock(isNowReachable);
            }
        }
        else
        {
            [self _startPendingTimer];
        }
    }
    else if ([timer isEqual:_monitoringTimer])
    {
        //for [self beginMonitoring]
        
        if(isNowReachable != _latestCheckWasReachable)
        {
            _latestCheckWasReachable = isNowReachable;
            NSString *notificationName = (isNowReachable) ? kNetworkReachabilityRestored : kNetworkReachabilityLost;
            [[NSNotificationCenter defaultCenter] postNotificationName:notificationName
                                                                object:nil];
            
            if (self.changeBlock)
            {
                self.changeBlock(isNowReachable);
            }
        }
    }
	
}

@end

@implementation NUNetworkReachabilityManager (NoInternetAlert)

- (void)showNetworkInaccessibleAlert
{
#if T_MACOS
    NSAlert *alert = [NSAlert new];
    alert.alertStyle = NSAlertStyleInformational;
    alert.messageText = kReachabilityNoInternetTitle;
    alert.informativeText = kReachabilityNoInternetMessage;
    [alert beginSheetModalForWindow:NSApp.mainWindow
                  completionHandler:^(NSModalResponse returnCode) {
                      
                  }];
#endif
    
#if T_IOS
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:kReachabilityNoInternetTitle
                                                        message:kReachabilityNoInternetMessage
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];

#endif
}

@end
