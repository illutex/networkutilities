//
//  NetworkReachabilityManager.h
//  NetworkUtilities
//
//  Created by Mike Ponomaryov on 07.08.2018.
//  Copyright © 2018 Mike Ponomaryov. All rights reserved.
//

@import Foundation;

extern NSString         *kNetworkReachabilityLost;
extern NSString         *kNetworkReachabilityRestored;

typedef void (^ReachabilityChangeHandler)(BOOL reachable);


@interface NUNetworkReachabilityManager : NSObject

@property (nonatomic, copy) ReachabilityChangeHandler   changeBlock;

+ (NUNetworkReachabilityManager *)shared;

//use it to quick check of the network reachability
+ (BOOL)isConnectedToNetwork;

//manages timer, which periodically checks for network reachability and posts notifications on
//state changes
- (void)beginMonitoring;
- (void)finishMonitoring;

//use it to check reacability and start pending timer that will continously check connection and will 
//send Notification in case of restoring
- (BOOL)checkNetworkReachability;

@end

@interface NUNetworkReachabilityManager (NoInternetAlert)

- (void)showNetworkInaccessibleAlert;

@end
