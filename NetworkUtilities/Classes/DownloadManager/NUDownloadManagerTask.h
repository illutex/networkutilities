//
//  DownloadManagerTask.h
//  NetworkUtilities
//
//  Created by Mike Ponomaryov on 07.08.2018.
//  Copyright © 2018 Mike Ponomaryov. All rights reserved.
//

#import <Foundation/Foundation.h>
#if TARGET_OS_IOS || TARGET_OS_WATCH
#import <UIKit/UIKit.h>
#else
#import <AppKit/AppKit.h>
#endif

#import "NUDownloadManager.h"

typedef void (^ProgressHandler)(NSNumber *progress);

@interface NUDownloadManagerTask : NSObject

@property (nonatomic, readonly) NSNumber                        *percent;
@property (nonatomic, readonly) NSURL                           *tempFileURL;
@property (nonatomic, readonly) NSString                        *serverFilename;
@property (nonatomic, strong)   NSString                        *downloadingPath;
@property (nonatomic, assign)   BOOL                            overwriteIfFileExists;
@property (nonatomic, copy)     ProgressHandler                 progressHandler;
@property (nonatomic, assign)   NUDownloadManagerTaskPriority   priority;

+ (instancetype)taskWithURL:(NSURL *)url;
- (instancetype)initWithURL:(NSURL *)url;

- (void)startDownloadWithCompletion:(void (^)(NSURL *localFileURL, NSError *error))completion;
- (void)startDownloadWithCompletion:(void (^)(NSURL *localFileURL, NSError *error))completion progressHandler:(ProgressHandler)progressHandler;
- (void)cancelDownload;

@end
