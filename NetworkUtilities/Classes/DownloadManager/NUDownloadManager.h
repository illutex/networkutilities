//
//  DownloadManager.h
//  NetworkUtilities
//
//  Created by Mike Ponomaryov on 07.08.2018.
//  Copyright © 2018 Mike Ponomaryov. All rights reserved.
//

#if T_IOS_OR_TVOS
@import UIKit;
#else
@import Foundation;
#endif

extern NSString *DMDidStartDownloadingNotification;
extern NSString *DMDidFinishDownloadingNotification;
extern NSString *DMDownloadingProgressNotification;
extern NSString *DMDidCompleteWithErrorNotification;

extern NSString *DMSourceKey;
extern NSString *DMIdentifierKey;
extern NSString *DMProgressKey;
extern NSString *DMLocationKey;
extern NSString *DMSuggestedFileNameKey;
extern NSString *DMErorKey;

typedef enum
{
    NUDownloadManagerTaskPriorityLow = 0,
    NUDownloadManagerTaskPriorityHigh
}
NUDownloadManagerTaskPriority;

@interface NUDownloadManager : NSObject

+ (instancetype)shared;

- (NSUInteger)downloadURL:(NSURL *)url; // uses high priority by default
- (NSUInteger)downloadURL:(NSURL *)url priority:(NUDownloadManagerTaskPriority)priority;
- (void)cancelTaskWithID:(NSUInteger)taskID;
- (void)cancelAllTasks;
- (BOOL)isDownloadTaskExists:(NSUInteger)taskID;

- (NSString *)downloadPath;

@end
