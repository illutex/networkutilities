//
//  DownloadManager.m
//  NetworkUtilities
//
//  Created by Mike Ponomaryov on 07.08.2018.
//  Copyright © 2018 Mike Ponomaryov. All rights reserved.
//

#import "NUDownloadManager.h"
#import <objc/runtime.h>
#import <CommonCrypto/CommonHMAC.h>

#define kSessionConfigID                        @"DownloadingBackgroundSession"
#define kMaxConnectionsPerHost                  3
#define kHighPriorityTasksMaxConnections        2
#define kLowPriorityTasksMinConnections         (kMaxConnectionsPerHost - kHighPriorityTasksMaxConnections)
#define kKeepAliveInterval                      30
#define kDownloadedFileExtension                @"tmp"

#define kDocumentsDir                           [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]
#define kCachesDir                              [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject]
#define kDownloadedFilesDirPath                 [kDocumentsDir stringByAppendingPathComponent:@"DownloadedFiles"]

NSString* DMDidStartDownloadingNotification     = @"DMDidStartDownloadingNotification";
NSString* DMDidFinishDownloadingNotification    = @"DMDidFinishDownloadingNotification";
NSString* DMDownloadingProgressNotification     = @"DMDownloadingProgressNotification";
NSString* DMDidCompleteWithErrorNotification    = @"DMDidCompleteWithErrorNotification";

NSString *DMSourceKey                           = @"source";
NSString *DMIdentifierKey                       = @"identifier";
NSString *DMProgressKey                         = @"progress";
NSString *DMLocationKey                         = @"location";
NSString *DMSuggestedFileNameKey                = @"suggestedFilename";
NSString *DMErorKey                             = @"error";

static NUDownloadManager*   sharedManager = nil;

@interface NUDownloadManager ()

@property (nonatomic, strong)   NSURLSession    *session;
@property (nonatomic, strong)   NSTimer         *sessionHeartBeatTimer;

@property (nonatomic, strong)   NSMutableArray  *lowPriorityTasks;
@property (nonatomic, strong)   NSMutableArray  *highPriorityTasks;

@end

@interface NUDownloadManager (NSURLSessionDelegate) <NSURLSessionDataDelegate, NSURLSessionDownloadDelegate> @end

@interface NSURLSessionDownloadTask (LastReceivedTime)

@property (nonatomic, copy)     NSDate          *lastDataReceivedTime;

@end

@implementation NSObject (LastReceivedTime)

- (void)setLastDataReceivedTime:(id)newLastDataReceivedTime
{
    objc_setAssociatedObject(self, @selector(lastDataReceivedTime), newLastDataReceivedTime, OBJC_ASSOCIATION_COPY);
}

- (id)lastDataReceivedTime
{
    return objc_getAssociatedObject(self, @selector(lastDataReceivedTime));
}

@end

@interface NSURL (MD5)

- (NSString *)md5;

@end

@implementation NSURL (MD5)

- (NSString *)md5
{
    // Borrowed from: http://stackoverflow.com/questions/652300/using-md5-hash-on-a-string-in-cocoa
    const char *cStr = [[self absoluteString] UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result);
    NSString *md5 = [NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",result[0], result[1], result[2], result[3], result[4], result[5], result[6], result[7],result[8], result[9], result[10], result[11],result[12], result[13], result[14], result[15]];
    return md5;
}

@end


@implementation NUDownloadManager

+ (instancetype)shared
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [NUDownloadManager new];
    });
    
    return sharedManager;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        NSURLSessionConfiguration* sessionCfg = nil;
        if ([NSURLSessionConfiguration respondsToSelector:@selector(backgroundSessionConfigurationWithIdentifier:)])
        {
            sessionCfg = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:kSessionConfigID];
        }
        else
        {
            sessionCfg = [NSURLSessionConfiguration defaultSessionConfiguration];
        }
        
        sessionCfg.allowsCellularAccess = YES;
        sessionCfg.timeoutIntervalForResource = 0;
        sessionCfg.timeoutIntervalForRequest = 0;
        sessionCfg.HTTPMaximumConnectionsPerHost = kMaxConnectionsPerHost;
        self.session = [NSURLSession sessionWithConfiguration:sessionCfg
                                                     delegate:self
                                                delegateQueue:[NSOperationQueue mainQueue]];
        
        self.lowPriorityTasks = [NSMutableArray array];
        self.highPriorityTasks = [NSMutableArray array];
        
        [self.session getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks)
        {
            [downloadTasks enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
            {
                NSURLSessionDownloadTask* dwnTask = obj;
                dwnTask.lastDataReceivedTime = [NSDate date];
                if ([dwnTask.taskDescription intValue] == NUDownloadManagerTaskPriorityHigh)
                {
                    [self.highPriorityTasks addObject:dwnTask];
                }
                else
                {
                    [self.lowPriorityTasks addObject:dwnTask];
                }
            }];
        }];
        
        __weak NUDownloadManager* weakSelf = self;
        self.sessionHeartBeatTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                                      target:weakSelf
                                                                    selector:@selector(_checkTasksAlive)
                                                                    userInfo:nil
                                                                     repeats:YES];
    }
    return self;
}

- (NSUInteger)downloadURL:(NSURL*)url
{
    return [self downloadURL:url priority:NUDownloadManagerTaskPriorityHigh];
}

- (NSUInteger)downloadURL:(NSURL*)url priority:(NUDownloadManagerTaskPriority)priority
{
    if (!url || url.isFileURL) return NSNotFound;
    
    NSURLRequest* taskRequest = [NSURLRequest requestWithURL:url];
    NSURLSessionDownloadTask* dwnTask = [self.session downloadTaskWithRequest:taskRequest];
    dwnTask.taskDescription = [NSString stringWithFormat:@"%d", priority];
    
    if (!dwnTask)
    {
        return NSNotFound;
    }
    if (priority == NUDownloadManagerTaskPriorityHigh)
    {
        [self.highPriorityTasks addObject:dwnTask];
    }
    else
    {
        [self.lowPriorityTasks addObject:dwnTask];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^
    {
        [self _executeTasks];
    });
    
    return dwnTask.taskIdentifier;
}

- (void)cancelTaskWithID:(NSUInteger)taskID
{
    [self.session getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks)
    {
        NSURLSessionDownloadTask* dwnTask = [self _findTaskInArray:downloadTasks byTaskID:taskID];
        if (dwnTask)
        {
            [self.highPriorityTasks removeObject:dwnTask];
            [self.lowPriorityTasks removeObject:dwnTask];
            [dwnTask cancel];
        }
        else
        {
            NSLog(@"Could not find task with ID: %lu", (unsigned long)taskID);
        }
    }];
}

- (void)cancelAllTasks
{
    [self.highPriorityTasks removeAllObjects];
    [self.lowPriorityTasks removeAllObjects];
    [self.session getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks)
    {
        [downloadTasks enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
        {
            NSURLSessionDownloadTask* dwnTask = obj;
            [dwnTask cancel];
        }];
    }];
    [self.session flushWithCompletionHandler:^
    {
        NSLog(@"all tasks cancelled");
    }];
}

- (BOOL)isDownloadTaskExists:(NSUInteger)taskID
{
    BOOL (^compareDownloadTasks)(id, NSUInteger, BOOL* ) = ^(id obj, NSUInteger idx, BOOL *stop)
    {
        *stop = ((NSURLSessionDownloadTask* )obj).taskIdentifier == taskID;
        return *stop;
    };
    NSUInteger idxOfLowTask = [self.lowPriorityTasks indexOfObjectPassingTest:compareDownloadTasks];
    NSUInteger idxOfHighTask = [self.highPriorityTasks indexOfObjectPassingTest:compareDownloadTasks];
    return idxOfLowTask != NSNotFound || idxOfHighTask != NSNotFound;
}

- (NSString *)downloadPath
{
    return kDownloadedFilesDirPath;
}

#pragma mark - Private methods

- (void)_executeTasks
{
    NSUInteger highPriorityTasksRunningCount = [self _calculateRunningTasks:self.highPriorityTasks];
    NSUInteger lowPriorityTasksRunningCount = [self _calculateRunningTasks:self.lowPriorityTasks];

    [self _runSuspendedTasks:self.highPriorityTasks inCount:kHighPriorityTasksMaxConnections - highPriorityTasksRunningCount];
    [self _runSuspendedTasks:self.lowPriorityTasks inCount:kLowPriorityTasksMinConnections - lowPriorityTasksRunningCount];
//    [self _printSystemStateDetailed:NO];
}

- (NSUInteger)_calculateRunningTasks:(NSArray*)tasks
{
    __block NSUInteger runningTasksCount = 0;
    [tasks enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
    {
        NSURLSessionDownloadTask* dwnTask = obj;
        if (dwnTask.state == NSURLSessionTaskStateRunning)
        {
            runningTasksCount++;
        }
    }];
    return runningTasksCount;
}

- (void)_runSuspendedTasks:(NSArray*)tasks inCount:(NSUInteger)needToStartCount
{
    __block NSUInteger startedCount = 0;
    [tasks enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
    {
        if (startedCount == needToStartCount)
        {
            *stop = YES;
        }
        else
        {
            NSURLSessionDownloadTask* dwnTask = obj;
            if (dwnTask.state == NSURLSessionTaskStateSuspended)
            {
                [dwnTask resume];
                dwnTask.lastDataReceivedTime = [NSDate date];
                startedCount++;
                
                NSURL* source = dwnTask.originalRequest.URL;
                NSString* taskID = [NSString stringWithFormat:@"%lu", (unsigned long)dwnTask.taskIdentifier];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:DMDidStartDownloadingNotification
                                                                    object:nil
                                                                  userInfo:@{@"source": source,
                                                                             @"identifier": taskID}];
            }
        }
    }];
//    DLog(@"%d started.", startedCount);
}

- (NSUInteger)_taskIDByURL:(NSURL*)url
{
    __block NSUInteger taskID = NSNotFound;
    [self.session getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks)
    {
        [downloadTasks enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
        {
            NSURLSessionDownloadTask* dwnTask = obj;
            if ([dwnTask.originalRequest.URL isEqual:url])
            {
                taskID = dwnTask.taskIdentifier;
                *stop = YES;
            }
        }];
    }];
    return taskID;
}

- (NSURLSessionDownloadTask *)_findTaskInArray:(NSArray *)tasksArray byTaskID:(NSUInteger)taskID
{
    __block NSURLSessionDownloadTask *task = nil;
    
    [tasksArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
    {
        NSURLSessionDownloadTask* dwnTask = obj;
        if (dwnTask.taskIdentifier == taskID)
        {
            task = dwnTask;
            *stop = YES;
        }
    }];
    
    return task;
}

- (void)_checkTasksAlive
{
    void (^cancelOldTask)(id, NSUInteger, BOOL*) = ^(id obj, NSUInteger idx, BOOL *stop)
    {
        NSURLSessionDownloadTask* dwnTask = obj;
        if (dwnTask.state == NSURLSessionTaskStateRunning)
        {
            NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:dwnTask.lastDataReceivedTime];
            if (secondsBetween >= kKeepAliveInterval)
            {
                [dwnTask cancel];
            }
        }
    };
    
    [self.highPriorityTasks enumerateObjectsUsingBlock:cancelOldTask];
    [self.lowPriorityTasks enumerateObjectsUsingBlock:cancelOldTask];
}

- (void)_printSystemStateDetailed:(BOOL)detailed
{
    [self.session getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks)
    {
        NSMutableString* msg = [NSMutableString string];
        [msg appendString:@"\n----System state----\n"];
        
        if (downloadTasks.count == 0)
        {
            [msg appendString:@"No tasks\n"];
        }
        else
        {
            NSUInteger tasksCount = downloadTasks.count;
            __block NSUInteger runningTasksCount = 0;
            __block NSUInteger suspendedTasksCount = 0;
            __block NSUInteger completedTasksCount = 0;
            __block NSUInteger cancelledTasksCount = 0;
            
            NSMutableString* detailedMsg = [NSMutableString string];
            [downloadTasks enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSURLSessionDownloadTask* task = (NSURLSessionDownloadTask*)obj;
                NSString* taskPriority = (task.taskDescription.intValue) ? @"High": @"Low";
                NSString* taskState = nil;
                switch (task.state)
                {
                    case 0:
                        taskState = @">>> NSURLSessionTaskStateRunning <<<";
                        runningTasksCount++;
                        break;
                    case 1:
                        taskState = @"NSURLSessionTaskStateSuspended";
                        suspendedTasksCount++;
                        break;
                    case 2:
                        taskState = @"NSURLSessionTaskStateCanceling";
                        cancelledTasksCount++;
                        break;
                    case 3:
                        taskState = @"NSURLSessionTaskStateCompleted";
                        completedTasksCount++;
                        break;
                    default:
                        break;
                }
                
                [detailedMsg appendFormat:@"task \t%lu: id = %lu,\t priority = %@,\t state = %@\n\n", (unsigned long)idx, (unsigned long)task.taskIdentifier, taskPriority, taskState];
            }];
            if (detailed)
            {
                [msg appendString:detailedMsg];
            }
            [msg appendFormat:@"Overall count\t = %lu\n", (unsigned long)tasksCount];
            [msg appendFormat:@"Running count\t = %lu\n", (unsigned long)runningTasksCount];
            [msg appendFormat:@"Suspended count\t = %lu\n", (unsigned long)suspendedTasksCount];
            [msg appendFormat:@"Cancelled count\t = %lu\n", (unsigned long)cancelledTasksCount];
            [msg appendFormat:@"Completed count\t = %lu\n", (unsigned long)completedTasksCount];
        }
        [msg appendString:@"--------------------\n"];
        NSLog(@"%@", msg);
    }];
}

- (NSString *)_saveDownloadedFile:(NSURL *)url from:(NSURL *)localPath
{
    NSString* sourceID = [url md5];
    NSString* downloadedFilesPath = [kDownloadedFilesDirPath stringByAppendingPathComponent:sourceID];
    NSFileManager* fm = [NSFileManager defaultManager];
    NSError* error = nil;
    if (![fm fileExistsAtPath:downloadedFilesPath])
    {
        [fm createDirectoryAtPath:downloadedFilesPath
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:&error];
        NSAssert(!error, @"Can't create a directory. Error: %@", error);
    }
    NSString* toPath = [downloadedFilesPath stringByAppendingPathComponent:localPath.lastPathComponent];
    [fm moveItemAtPath:localPath.path toPath:toPath error:&error];
    NSAssert(!error, @"Can't move a file. Error: %@", error);
    return toPath;
}

- (NSString *)_downloadedFileFrom:(NSURL *)url
{
    NSString* sourceID = [url md5];
    NSString* downloadedFilesPath = [kDownloadedFilesDirPath stringByAppendingPathComponent:sourceID];
    NSFileManager* fm = [NSFileManager defaultManager];
    NSError* error;
    NSArray* content = [fm contentsOfDirectoryAtPath:downloadedFilesPath error:&error];
    NSAssert(!error, @"Can't find a directory. Error: %@", error);
    NSIndexSet* indexes = [content indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop)
    {
        return [[(NSString* )obj pathExtension] isEqualToString:kDownloadedFileExtension] ? YES : NO;
    }];
    NSArray* downloadedFiles = [content objectsAtIndexes:indexes];
    if (downloadedFiles.count > 0)
    {
        return downloadedFiles[0];
    }
    return nil;
}

@end

#pragma mark - NSURLSessionDelegate

@implementation NUDownloadManager (NSURLSessionDelegate)

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    [self.highPriorityTasks removeObject:task];
    [self.lowPriorityTasks removeObject:task];
    [self _executeTasks];
    
    if (error)
    {
        // DLog(@"Task %lu: Error (%@)", (unsigned long)task.taskIdentifier, error);
        NSURL* source = task.originalRequest.URL;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:DMDidCompleteWithErrorNotification
                                                            object:nil
                                                          userInfo:@{DMSourceKey: source,
                                                                     DMIdentifierKey: [NSNumber numberWithUnsignedInteger:task.taskIdentifier],
                                                                     DMErorKey: error}];
    }
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    NSURL* source = downloadTask.originalRequest.URL;
    
    NSString *serverFilename = downloadTask.response.suggestedFilename;
    if (!serverFilename) serverFilename = location.lastPathComponent;

    [[NSNotificationCenter defaultCenter] postNotificationName:DMDidFinishDownloadingNotification
                                                        object:nil
                                                      userInfo:@{DMLocationKey: location,
                                                                 DMSourceKey: source,
                                                                 DMIdentifierKey: [NSNumber numberWithUnsignedInteger:downloadTask.taskIdentifier],
                                                                 DMSuggestedFileNameKey: serverFilename}];
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
                                           didWriteData:(int64_t)bytesWritten
                                      totalBytesWritten:(int64_t)totalBytesWritten
                              totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    downloadTask.lastDataReceivedTime = [NSDate date];
    NSURL* source = downloadTask.originalRequest.URL;
    
    double progress = (totalBytesExpectedToWrite >= 0) ? (double)totalBytesWritten / (double)totalBytesExpectedToWrite : 0.0f;
    //DLog(@"taskID: %lu, percentage: %f", (unsigned long)downloadTask.taskIdentifier, progress);
    NSNumber *percentNumber = [NSNumber numberWithDouble:progress];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:DMDownloadingProgressNotification
                                                        object:nil
                                                      userInfo:@{DMSourceKey: source,
                                                                 DMProgressKey: percentNumber,
                                                                 DMIdentifierKey: [NSNumber numberWithUnsignedInteger:downloadTask.taskIdentifier]}];
}

@end
