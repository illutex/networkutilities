//
//  MOperation+Queue.h
//  NetworkUtilities
//
//  Created by Mike Ponomaryov on 07.08.2018.
//  Copyright © 2018 Mike Ponomaryov. All rights reserved.
//

#import "NUOperation.h"

typedef void (^NUOperationQueueCompletionBlock)(NUOperation *operation);

@interface NUOperation (Queue)

@property (nonatomic, copy)     NUOperationQueueCompletionBlock      queueCompletionHandler;

@end
