//
//  MOperation.h
//  NetworkUtilities
//
//  Created by Mike Ponomaryov on 07.08.2018.
//  Copyright © 2018 Mike Ponomaryov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NUOperation;

typedef enum : NSUInteger {
    NUOperationStatusIdle,
    NUOperationStatusExecuting,
    NUOperationStatusFinished,
    NUOperationStatusCancelled,
} NUOperationStatus;

typedef void (^NUOperationExecutionBlock)(NUOperation *operation);
typedef void (^NUOperationCompletionHandler)(NUOperation *operation);
typedef void (^NUOperationCancellationHandler)(NUOperation *operation);

@interface NUOperation : NSObject

@property (nonatomic)   NUOperationStatus   status;
@property (nonatomic)   id                  associatedObject;

- (instancetype)init NS_UNAVAILABLE;
- (id)copy NS_UNAVAILABLE;

+ (instancetype)operationWithExecutionBlock:(NUOperationExecutionBlock)executionBlock;
+ (instancetype)operationWithExecutionBlock:(NUOperationExecutionBlock)executionBlock
                          completionHandler:(NUOperationCompletionHandler)completionHandler
                        cancellationHandler:(NUOperationCancellationHandler)cancellationHandler;

@end
