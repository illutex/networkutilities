//
//  MOperationQueue.m
//  NetworkUtilities
//
//  Created by Mike Ponomaryov on 07.08.2018.
//  Copyright © 2018 Mike Ponomaryov. All rights reserved.
//

#import "NUOperationQueue.h"
#import "NUOperation+Queue.h"

@interface NUOperationQueue ()

@property (readwrite)           BOOL                                isExecuting;
@property (readwrite)           NSMutableArray <NUOperation *>      *operations;

@property (nonatomic, copy)     NUOperationQueueCompletionHandler    completionHandler;

@end

@interface NUOperationQueue (Memory)

- (void)hold;
- (void)unhold;

@end

static NSMutableArray <NUOperationQueue *> *staticQueues = nil;

@implementation NUOperationQueue

@synthesize simultaneousExecution = _simultaneousExecution;

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.operations = [NSMutableArray array];
        self.isExecuting = NO;
    }
    return self;
}

- (void)addOperation:(NUOperation *)operation
{
    NSAssert(!(self.simultaneousExecution && self.isExecuting), @"Can't add operation while execution is in progress");
    
    [((NSMutableArray *)self.operations) addObject:operation];
}

- (void)addOperations:(NSArray <NUOperation *> *)operations
{
    NSAssert(!(self.simultaneousExecution && self.isExecuting), @"Can't add operations while execution is in progress");
    
    [((NSMutableArray *)self.operations) addObjectsFromArray:operations];
}

- (void)removeOperation:(NUOperation *)operation
{
    NSAssert(!(self.simultaneousExecution && self.isExecuting), @"Can't remove operation while execution is in progress");
    
    if ([self.operations indexOfObject:operation] != NSNotFound)
    {
        [((NSMutableArray *)self.operations) removeObject:operation];
    }
}

- (void)removeOperations:(NSArray <NUOperation *> *)operations
{
    for (NUOperation *operation in operations)
    {
        [self removeOperation:operation];
    }
}

- (void)startWithCompletionHandler:(NUOperationQueueCompletionHandler)completionHandler
{
    if (!self.operations.count || self.isExecuting)
    {
        return;
    }
    
    self.completionHandler = completionHandler;
    self.isExecuting = YES;
    
    [self hold];
    
    if (self.simultaneousExecution)
    {
        [self _startAllOperations];
    }
    else
    {
        [self _startOperationQueueFromOperation:self.operations.firstObject];
    }
}

- (void)stop
{
    self.completionHandler = nil;
    
    for (NUOperation *operation in self.operations)
    {
        if (operation.status == NUOperationStatusExecuting || operation.status == NUOperationStatusIdle)
        {
            operation.queueCompletionHandler = nil;
            operation.status = NUOperationStatusCancelled;
        }
    }
    [((NSMutableArray *)self.operations) removeAllObjects];
    
    self.isExecuting = NO;
}

- (BOOL)simultaneousExecution
{
    return _simultaneousExecution;
}

- (void)setSimultaneousExecution:(BOOL)simultaneousExecution
{
    NSAssert(!self.isExecuting, @"Can't change execution mode while its in progress");
    
    _simultaneousExecution = simultaneousExecution;
}

#pragma mark - Private

- (void)_startAllOperations
{
    __weak typeof(self) weakSelf = self;
    for (NUOperation *operation in self.operations)
    {
        operation.queueCompletionHandler = ^(NUOperation *operation) {
            BOOL everythingCompleted = [weakSelf _operationsCompleted];

            if (everythingCompleted)
            {
                weakSelf.isExecuting = NO;
                if (weakSelf.completionHandler) weakSelf.completionHandler(weakSelf);
                
                [weakSelf unhold];
            }
        };
        
        operation.status = NUOperationStatusExecuting;
    }
}

- (void)_startOperationQueueFromOperation:(NUOperation *)operation
{
    __weak typeof(self) weakSelf = self;

    operation.queueCompletionHandler = ^(NUOperation *operation) {
        NSUInteger index = [weakSelf.operations indexOfObject:operation];
        if (index != weakSelf.operations.count - 1)
        {
            [weakSelf _startOperationQueueFromOperation:weakSelf.operations[index + 1]];
        }
        else
        {
            weakSelf.isExecuting = NO;
            if (weakSelf.completionHandler) weakSelf.completionHandler(weakSelf);
            
            [weakSelf unhold];
        }
    };

    operation.status = NUOperationStatusExecuting;
}

- (BOOL)_operationsCompleted
{
    BOOL operationsCompleted = YES;
    for (NUOperation *operation in self.operations)
    {
        if (operation.status == NUOperationStatusExecuting || operation.status == NUOperationStatusIdle)
        {
            operationsCompleted = NO;
            break;
        }
    }
    
    return operationsCompleted;
}

@end

@implementation NUOperationQueue (Memory)

- (void)hold
{
    @synchronized (staticQueues)
    {
        if (!staticQueues)
        {
            staticQueues = [NSMutableArray array];
        }
        [staticQueues addObject:self];
    }
}

- (void)unhold
{
    @synchronized (staticQueues)
    {
        [staticQueues removeObject:self];
        if (staticQueues.count == 0)
        {
            staticQueues = nil;
        }
    }
}

@end
