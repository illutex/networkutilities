//
//  MOperation.m
//  NetworkUtilities
//
//  Created by Mike Ponomaryov on 07.08.2018.
//  Copyright © 2018 Mike Ponomaryov. All rights reserved.
//

#import "NUOperation.h"

typedef void (^NUOperationQueueCompletionBlock)(NUOperation *operation);

@interface NUOperation ()

@property (nonatomic, copy)     NUOperationExecutionBlock            executionBlock;
@property (nonatomic, copy)     NUOperationCompletionHandler         completionHandler;
@property (nonatomic, copy)     NUOperationCancellationHandler       cancellationHandler;

@property (nonatomic, copy)     NUOperationQueueCompletionBlock      queueCompletionHandler;

@end

@implementation NUOperation

@synthesize status = _status;

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.status = NUOperationStatusIdle;
    }
    return self;
}

+ (instancetype)operationWithExecutionBlock:(NUOperationExecutionBlock)executionBlock
{
    return [self operationWithExecutionBlock:executionBlock completionHandler:nil cancellationHandler:nil];
}

+ (instancetype)operationWithExecutionBlock:(NUOperationExecutionBlock)executionBlock completionHandler:(NUOperationCompletionHandler)completionHandler cancellationHandler:(NUOperationCancellationHandler)cancellationHandler
{
    NUOperation *operation = [self new];
    operation.executionBlock = executionBlock;
    operation.completionHandler = completionHandler;
    operation.cancellationHandler = cancellationHandler;
    
    return operation;
}

- (NUOperationStatus)status
{
    return _status;
}

- (void)setStatus:(NUOperationStatus)status
{
    if (status != _status)
    {
        _status = status;
        
        __weak typeof(self) weakSelf = self;
        switch (status)
        {
            case NUOperationStatusIdle:
                
                break;
                
            case NUOperationStatusExecuting:
                if (self.executionBlock) self.executionBlock(weakSelf);
                break;
                
            case NUOperationStatusFinished:
                if (self.completionHandler) self.completionHandler(weakSelf);
                if (self.queueCompletionHandler) self.queueCompletionHandler(weakSelf);
                break;
                
            case NUOperationStatusCancelled:
                if (self.cancellationHandler) self.cancellationHandler(weakSelf);
                if (self.queueCompletionHandler) self.queueCompletionHandler(weakSelf);
                break;
                
            default:
                break;
        }
    }
}

@end
