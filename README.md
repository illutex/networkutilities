# NetworkUtilities

[![CI Status](https://img.shields.io/travis/Mike Ponomaryov/NetworkUtilities.svg?style=flat)](https://travis-ci.org/Mike Ponomaryov/NetworkUtilities)
[![Version](https://img.shields.io/cocoapods/v/NetworkUtilities.svg?style=flat)](https://cocoapods.org/pods/NetworkUtilities)
[![License](https://img.shields.io/cocoapods/l/NetworkUtilities.svg?style=flat)](https://cocoapods.org/pods/NetworkUtilities)
[![Platform](https://img.shields.io/cocoapods/p/NetworkUtilities.svg?style=flat)](https://cocoapods.org/pods/NetworkUtilities)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NetworkUtilities is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'NetworkUtilities'
```

## Author

Mike Ponomaryov, dev.mikesoft@gmail.com

## License

NetworkUtilities is available under the MIT license. See the LICENSE file for more info.
