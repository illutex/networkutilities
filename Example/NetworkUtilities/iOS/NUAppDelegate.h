//
//  NUAppDelegate.h
//  NetworkUtilities
//
//  Created by Mike Ponomaryov on 07.08.2018.
//  Copyright © 2018 Mike Ponomaryov. All rights reserved.
//

@import UIKit;

@interface NUAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
