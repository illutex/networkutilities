//
//  NUViewController.m
//  NetworkUtilities
//
//  Created by Mike Ponomaryov on 07.08.2018.
//  Copyright © 2018 Mike Ponomaryov. All rights reserved.
//

#import "NUViewController.h"
#import "NUOperationQueue.h"
#import "NUDownloadManagerTask.h"
#import "NUNetworkReachabilityManager.h"

#define kFileURL1 @"https://touchcast.com/assetsv0/homepage/video/homepage-1.mp4"
#define kFileURL2 @"https://touchcast.com/assetsv0/homepage/video/webinar-1.mp4"

@implementation NUViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self _constantlyCheckNetworkReachability];
    [self _downloadSingleFile];
//    [self _downloadFilesInQueue];
}

#pragma mark - Private

- (void)_constantlyCheckNetworkReachability
{
    [NUNetworkReachabilityManager.shared beginMonitoring];
    NUNetworkReachabilityManager.shared.changeBlock = ^(BOOL isReachable)
    {
        NSLog(@"network reachable: %d", isReachable);
    };
}

- (void)_downloadSingleFile
{
    NSURL *url = [NSURL URLWithString:kFileURL1];

    [[NUDownloadManagerTask taskWithURL:url] startDownloadWithCompletion:^(NSURL *localFileURL, NSError *error) {
        if (error)
        {
            NSLog(@"error: %@", error.localizedDescription);
        }
        else
        {
            NSLog(@"filepath: %@", localFileURL.absoluteString);
        }
    } progressHandler:^(NSNumber *progress) {
        NSLog(@"progress: %f", progress.floatValue);
    }];
}

- (void)_downloadFilesInQueue
{
    NSArray <NSURL *> *files = @[[NSURL URLWithString:kFileURL1],
                                 [NSURL URLWithString:kFileURL2],
                                 [NSURL URLWithString:kFileURL1],
                                 [NSURL URLWithString:kFileURL2],
                                 [NSURL URLWithString:kFileURL1],
                                 [NSURL URLWithString:kFileURL2]];
    
    for (int queueIndex = 0; queueIndex < 2; queueIndex++)
    {
        NUOperationQueue *queue = [NUOperationQueue new];
//        queue.simultaneousExecution = YES;
        
        NSMutableArray <NUOperation *> *operations = [NSMutableArray array];
        for (int operationIndex = 0; operationIndex < files.count; operationIndex++)
        {
            NUOperation *operation = [NUOperation operationWithExecutionBlock:^(NUOperation *operation) {
                NSLog(@"operation %d executing (queue %d)", operationIndex, queueIndex);
                
                [[NUDownloadManagerTask taskWithURL:files[operationIndex]] startDownloadWithCompletion:^(NSURL *localFileURL, NSError *error) {
                    if (error)
                    {
                        NSLog(@"error: %@", error.localizedDescription);
                    }
                    else
                    {
                        NSLog(@"filepath: %@", localFileURL.absoluteString);
                    }
                    
                    NSLog(@"operation %d is finishing (queue %d)", operationIndex, queueIndex);
                    operation.status = NUOperationStatusFinished;
                    
                } progressHandler:^(NSNumber *progress) {
                    NSLog(@"progress: %f", progress.floatValue);
                }];
                
            } completionHandler:^(NUOperation *operation) {
                NSLog(@"operation %d finished (queue %d)", operationIndex, queueIndex);
            } cancellationHandler:^(NUOperation *operation) {
                NSLog(@"operation %d cancelled (queue %d)", operationIndex, queueIndex);
            }];
            [operations addObject:operation];
        }
        
        [queue addOperations:operations];
        
        [queue startWithCompletionHandler:^(NUOperationQueue *operationQueue) {
            NSLog(@"queue %d: execution finished", queueIndex);
        }];
    }
}

@end
