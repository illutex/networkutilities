//
//  AppDelegateOSX.h
//  NetworkUtilities
//
//  Created by Mike Ponomaryov on 07.08.2018.
//  Copyright © 2018 Mike Ponomaryov. All rights reserved.
//

#import <AppKit/AppKit.h>
#import <Foundation/Foundation.h>

@interface AppDelegateOSX : NSObject <NSApplicationDelegate>

@end
