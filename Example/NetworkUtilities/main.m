//
//  main.m
//  NetworkUtilities
//
//  Created by Mike Ponomaryov on 07.08.2018.
//  Copyright © 2018 Mike Ponomaryov. All rights reserved.
//

#if T_MACOS
#import "AppDelegateOSX.h"

int main(int argc, const char * argv[])
{
    @autoreleasepool {
        return NSApplicationMain(argc, argv);
    }
}
#else
#import "NUAppDelegate.h"
    
int main(int argc, char * argv[__nonnull])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NUAppDelegate class]));
    }
}
#endif
