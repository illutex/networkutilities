#ifdef __OBJC__
    #if T_IOS_OR_TVOS
    @import UIKit;
    @import Foundation;
    #else
    //@import AppKit;
    @import Foundation;
    #endif

    #import "NUDownloadManager.h"
    #import "NUDownloadManagerTask.h"
    #import "NUJSONDataRequest.h"
    #import "NUDataRequest.h"
    #import "NUNetworkReachabilityManager.h"
    #import "NUOperationQueue.h"

#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif


FOUNDATION_EXPORT double NetworkUtilitiesVersionNumber;
FOUNDATION_EXPORT const unsigned char NetworkUtilitiesVersionString[];
